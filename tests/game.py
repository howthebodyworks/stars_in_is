#!/usr/bin/env python
# encoding: utf-8
"""
game.py

Created by dan mackinlay on 2011-09-11.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""

import unittest
from hydra.game import Game

game_tests = unittest.TestSuite()

class BaseGame(unittest.TestCase):
    def setUp(self):
        pass

    def testRootGame(self):
        """basic sanity check that the base Game does what we want"""
        g1 = Game(mean_payoff=0)
        #root Game is invariant with strategy
        self.assertEqual(g1.play([1]*5), [0]*5)
        self.assertEqual(g1.play([0]*5), [0]*5)
        g2 = Game(mean_payoff=1.5)
        self.assertEqual(g2.play([1]*5), [1.5]*5)
        self.assertEqual(g2.play([0]*5), [1.5]*5)
        
        
#game_tests.addTest(BaseGame())

if __name__ == '__main__':
    unittest.main()