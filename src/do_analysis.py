#!/usr/bin/env python
# encoding: utf-8
"""
Sets up a nice environment for doing data analysis.
"""
import argparse
# from settings import *
import sys
from lib.dynamic import get_callable
import storage
import trial_plots

exp=None
plots=None

def parser_factory():
    parser = argparse.ArgumentParser(
      prog="do_analysis",
      description="look at the previous experiment")
    parser.add_argument('--experiment-id',
      type=str,
      default=None,
      help='experiment id, for finding your results later. Defaults to experiment name.')
    return parser
    
def main(str_vers=None, *cl_args):
    """gets our sophisticated parser and work out the invocation args"""
    
    parser = parser_factory()
    
    #but first we allow non CLI invocation as well, for ease of profiling,
    #by allowing us to override CLI args
    if str_vers:
        #the first arg, if it exists, is parsed as with a CLI string
        import shlex
        str_vers = shlex.split(str_vers)
        str_vers.extend(cl_args)
        cl_args = str_vers
        
    if not cl_args:
        cl_args = sys.argv[1:]
    
    args = parser.parse_args(cl_args)
    arg_string = ' '.join(cl_args)

    #now do some legwork to dispatch the arguments to the appropriate function
    args_dict = dict(args._get_kwargs())
    args_dict['command_line'] = arg_string

    return analyse_experiment(**args_dict)
    
def analyse_experiment(experiment_id,
        *args, **kwargs):
    
    global exp
    global plots
    
    exp = storage.ExperimentStorage(experiment_id)

    # if trial_plot_set:
    #     plot_fns = {}
    #     for f in trial_plot_set:
    #         plot_fns[f] = get_callable(f)
    #     for key in keys:
    #         trial = exp.get(key)
    #         for f in plot_fns.itervalues():
    #             f(trial.data)
    # 
    # 
if __name__ == "__main__":
   main()


