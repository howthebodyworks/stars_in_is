#!/usr/bin/env python
# encoding: utf-8
"""
A central control room wherefrom all commands are launched - not out of logical 
necessity, but for semantic clarity, and ease of data tracking with, e.g. 
sumatra.
"""
import argparse
# from settings import *
import sys
from lib.bunch import bunchify, Bunch
import dispatch

def embedded_shell():
    """This is slow, so I don't load it unless neccessary
    """
    import IPython
    return IPython.embed

def parser_factory():
    parser = argparse.ArgumentParser(
      prog="stars_in_is",
      description="simulate some traders in labour market",
      help='Run some simulation experiment for one parameter set')
    parser.add_argument('--arg-file',
      type=str,
      default=None,
      help='optional file full of serialized arguments')
    parser.add_argument('--experiment-id',
      type=str,
      default=None,
      help='experiment id, for finding your results later.')
    parser.add_argument('--key',
        type=str,
        default=None,
        help='trial id, for finding your results later.')
    parser.add_argument('--seed',
      type=int,
      help="seed for the trial.",
      default=0)
    parser.add_argument('--params', type=str, default='',
      help="override a sim param from the command line")
    parser.add_argument('--interactive', action='store_true',
      default=False, help="open a shell upon completion")
    parser.add_argument('--display-plots', action='store_true',
      default=False, help="enable GUI graphics")
    parser.add_argument('--seed-offset',
      type=int,
      help="seed for the first trial. subsequent seeds are generated in"
      " linear progression.",
      default=0)
    
    return parser

def main(str_vers=None, *cl_args):
    """gets our sophisticated parser and work out the invocation args"""
    
    parser = parser_factory()
    
    #but first we allow non CLI invocation as well, for ease of profiling,
    #by allowing us to override CLI args
    if str_vers:
        #the first arg, if it exists, is parsed as with a CLI string
        import shlex
        str_vers = shlex.split(str_vers)
        str_vers.extend(cl_args)
        cl_args = str_vers
        
    if not cl_args:
        cl_args = sys.argv[1:]
    
    args = parser.parse_args(cl_args)
    arg_string = ' '.join(cl_args)

    #now do some legwork to dispatch the arguments to the appropriate function
    args_dict = dict(args._get_kwargs())
    args_dict['command_line'] = arg_string

    return simulate_trial(**args_dict)

def simulate_trial(
        arg_file=None,
        key=None,
        no_cleanup=False,
        interactive=False,
        cli_params='',
        seed=None,
        verbosity=0,
        trial_params=None,
        display_plots=False,
        *args, **kwargs):
    """For CLI invocation, we can write stuff to disk from here"""
    import exp_settings
    #automatically get cPickle:
    from lib.util import pickle
    from storage import ExperimentStorage
    
    if trial_params is None:
        trial_params = {}
    
    # do this with exec to parse python from the CLI
    # obviously this is hopelesssly insecure
    exec('cli_params = dict(%s)' % kwargs.pop('cli_params', ''))
    trial_params.update(cli_params)
    
    #Only pass non-null parameters; else let it choose its own default
    for k, v in kwargs.items():
        if v is None:
            del(kwargs[k])
    
    if arg_file is not None:
        #allow us to pass in serialized args
        with open(arg_file, 'rb') as f:
            trial_params.update(pickle.load(f))
    
    trial_params.update(kwargs)
    
    storage = ExperimentStorage(trial_params['experiment_id'])
    
    key = dispatch.write_to_file(
        storage=storage,
        trial_params=trial_params,
        verbosity=verbosity,
        key=key)
    
    if interactive:
        embedded_shell()()
        
    return trial_params['experiment_id'], key

if __name__ == "__main__":
   main()


