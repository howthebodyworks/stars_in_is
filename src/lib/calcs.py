#!/usr/bin/env python
# encoding: utf-8
"""
math.py

Created by dan mackinlay on 2012-01-15.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""
from __future__ import division
import math

def half_life_steps_to_ratio(steps):
    if steps==float('inf'):
        return 1.0
    return math.exp(math.log(0.5)/steps)

def affine_cipher(integer):
    return (1664525* integer + 1013904223) % (2**32)
    
def double_affine_cipher(integer):
    return affine_cipher(affine_cipher(integer))