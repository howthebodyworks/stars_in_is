#!/usr/bin/env python
# encoding: utf-8
"""
dynamic.py

Cribbed from Django; functions to allow django-style string imports, and function currying for dynamics swapping in of objects

Used under the BSD licence of the Django codebase.

Copyright 2011 Daniel MacKinaly and the original authors.
"""

from importlib import import_module
from functools import wraps, update_wrapper

_callable_cache = {} 

# You can't trivially replace this `functools.partial` because this binds to
# classes and returns bound instances, whereas functools.partial (on CPython)
# is a type and its instances don't bind.
def curry(_curried_func, *args, **kwargs):
    def _curried(*moreargs, **morekwargs):
        return _curried_func(*(args+moreargs), **dict(kwargs, **morekwargs))
    return _curried

def memoize(func, cache, num_args):
    """
    Wrap a function so that results for any argument tuple are stored in
    'cache'. Note that the args to the function must be usable as dictionary
    keys.

    Only the first num_args are considered when creating the key.
    """
    @wraps(func)
    def wrapper(*args):
        mem_args = args[:num_args]
        if mem_args in cache:
            return cache[mem_args]
        result = func(*args)
        cache[mem_args] = result
        return result
    return wrapper
    
def get_mod_func(callback):
    # Converts 'django.views.news.stories.story_detail' to
    # ['django.views.news.stories', 'story_detail']
    try:
        dot = callback.rindex('.')
    except ValueError:
        return callback, ''
    return callback[:dot], callback[dot+1:]
    
def get_callable(callable_path):
    """
    Convert a string version of a function name to the callable object.
    """
    if not callable(callable_path):
        mod_name, func_name = get_mod_func(callable_path)
        if func_name != '':
            callable_path = getattr(import_module(mod_name), func_name)
            if not callable(callable_path):
                raise ImportError(
                    "Could not import %s.%s. Not a callable." %
                    (mod_name, func_name))
    return callable_path
get_callable = memoize(get_callable, _callable_cache, 1)

def module_has_submodule(package, module_name):
    """See if 'module' is in 'package'."""
    name = ".".join([package.__name__, module_name])
    try:
        # None indicates a cached miss; see mark_miss() in Python/import.c.
        return sys.modules[name] is not None
    except KeyError:
        pass
    try:
        package_path = package.__path__   # No __path__, then not a package.
    except AttributeError:
        # Since the remainder of this function assumes that we're dealing with
        # a package (module with a __path__), so if it's not, then bail here.
        return False
    for finder in sys.meta_path:
        if finder.find_module(name, package_path):
            return True
    for entry in package_path:
        try:
            # Try the cached finder.
            finder = sys.path_importer_cache[entry]
            if finder is None:
                # Implicit import machinery should be used.
                try:
                    file_, _, _ = imp.find_module(module_name, [entry])
                    if file_:
                        file_.close()
                    return True
                except ImportError:
                    continue
            # Else see if the finder knows of a loader.
            elif finder.find_module(name):
                return True
            else:
                continue
        except KeyError:
            # No cached finder, so try and make one.
            for hook in sys.path_hooks:
                try:
                    finder = hook(entry)
                    # XXX Could cache in sys.path_importer_cache
                    if finder.find_module(name):
                        return True
                    else:
                        # Once a finder is found, stop the search.
                        break
                except ImportError:
                    # Continue the search for a finder.
                    continue
            else:
                # No finder found.
                # Try the implicit import machinery if searching a directory.
                if os.path.isdir(entry):
                    try:
                        file_, _, _ = imp.find_module(module_name, [entry])
                        if file_:
                            file_.close()
                        return True
                    except ImportError:
                        pass
                # XXX Could insert None or NullImporter
    else:
        # Exhausted the search, so the module cannot be found.
        return False

def get_curried_callable(callable_string_or_tuple):
    """ Given a callable string, or a tuple of callable string and kwargs,
    return a curried callable. If passed a callable, give it straight back, to
    keep this idempotent."""
    # Callables
    if callable(callable_string_or_tuple):
        return callable_string_or_tuple
    # Strings
    if isinstance(callable_string_or_tuple, basestring):
        return get_callable(callable_string_or_tuple)
    # Other. Assume tuple/list/iterable of string and kwargs.
    fn = get_callable(callable_string_or_tuple[0])
    return curry(fn, **callable_string_or_tuple[1])
    