#!/usr/bin/env python
# encoding: utf-8
"""
util.py

Created by dan mackinlay on 2011-12-27.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""
import hashlib
import os

try:
    import cPickle as pickle
except ImportError:
    import pickle

def embedded_shell():
    """This is slow, so I don't load it unless neccessary
    """
    import IPython
    return IPython.embed

def trace():
    from IPython.core.debugger import Tracer
    return Tracer()

def digest(obj):
    """return a hex digest of a picklable object
    Since pickle's error messages are awful, we re-raise the error
    with more info."""
    try:
        return hashlib.sha224(pickle.dumps(obj)).hexdigest()
    except Exception, exc:
        our_exc = exc.__class__(
          'Got "%s" when pickling %s' % (repr(exc), repr(obj)))
        our_exc.obj = obj
        raise our_exc

def makedirs(path, mode=0777):
    try:
        os.makedirs(path, mode)
    except OSError, e:
        # no problem if it already exists
        if e.errno==17:
            pass
        else:
            raise e

