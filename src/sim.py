#!/usr/bin/env python
# encoding: utf-8
"""
sim.py

Created by dan mackinlay on 2011-11-17.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.

Where the main loop of the simulation is executed.
"""
from __future__ import division
from lib.dynamic import get_callable
from lib.bunch import Bunch, bunchify
from world import World
import init
from exp_settings import DEFAULT_PARAMS
import random
from lib.util import trace
from engine import Engine

def main(params, verbosity=0):
    # support lotsa calling conventions by merging params
    # then merging with default args. A bit messy.
    _ = DEFAULT_PARAMS.copy()
    _.update(params)
    params = bunchify(_)
    del(_)
    
    # First, seed RNGs
    seed = params.seed
    random.seed(seed)
    
    #Now, get the right functions loaded.
    worker_factory = get_callable(
        params.worker_factory)
    firm_factory = get_callable(
        params.firm_factory)
    firm_culture_fn = get_callable(
        params.firm_culture_fn)
    firm_knowledge_fn = get_callable(
        params.firm_knowledge_fn)
    firm_competitiveness_fn = get_callable(
        params.firm_competitiveness_fn)
    firm_returns_fn = get_callable(
        params.firm_returns_fn)
    worker_canonical_valuator = get_callable(
        params.worker_canonical_valuator)
    firm_private_worker_valuator = get_callable(
        params.firm_private_worker_valuator)
    hiring_fn = get_callable(
        params.hiring_fn)
    valuation_lagger = get_callable(
        params.worker_canonical_valuation_lagger)
    firm_info_updater = get_callable(
        params.firm_info_updater)
    
    # create our agents
    world = World(params=params, engine=Engine(verbosity=verbosity))
    world.engine.message("running sim with params")
    world.engine.pmessage(params)
    workers = worker_factory(world=world, params=params)
    firms = firm_factory(world=world, params=params)
    world.add_firms(firms)
    world.add_workers(workers)
    
    #allocate workers to firms
    for i, worker in enumerate(workers):
        worker.join(firms[i%params.num_firms])
    
    #initialise worker tracker storage
    if hasattr(firm_info_updater, 'setup'):
        firm_info_updater.setup(world)
    
    #main sim loop
    for i in world:
        #Firms run their businesses
        firm_competitiveness_fn(world, params)
        for i, ret in enumerate(firm_returns_fn(firms, params)):
            firm = firms[i]
            firm.do_accounts(income=ret)
        
        #Information about performance is distributed
        worker_canonical_valuator(world)
        valuation_lagger(world, params)
        
        #information about state fo the world/workes is collected/updated
        firm_info_updater(world)
        
        #Firms hire/sack their staff
        hiring_fn(world)
        
    # import pdb; pdb.set_trace()

    return world