#!/usr/bin/env python
# encoding: utf-8
"""
settings.py

Created by dan mackinlay on 2011-12-27.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""

EXP_PATH = 'Data/'
PARAM_PATH = 'Params/'
OUTPUT_PATH = 'Output/'

SKIP_STEPS = 10