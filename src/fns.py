#!/usr/bin/env python
# encoding: utf-8
"""
fns.py

Created by dan mackinlay on 2011-11-18.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.

All the swappable workhorse functions that make the simulation go.
"""
from __future__ import division
from collections import namedtuple
from random import randrange, random, sample
from lib.dynamic import get_callable
from vector_arithmetic import mean, geom_mean, rand_bool_vector
from lib.bunch import Bunch, bunchify
from agents import Worker, Firm
from lib.calcs import half_life_steps_to_ratio
from math import log, exp
from lib.util import trace

################################################################
# Null function
def null(*args, **kwargs):
    return

################################################################
# Agent factories
def random_workers(world, number=None, params=None):
    if number is None:
        number = params.num_workers
        
    return tuple([
        Worker(
           culture = rand_bool_vector(
               params.culture_length,
               params.culture_density),
           knowledge = random(),
           wage = 0.0,
           canonical_valuation = 0.0
        ) for i in xrange(number)])

def random_firms(world, number=None, params=None):
    if number is None:
        number = params.num_firms
    initial_funds = float(params.get('firm_initial_funds_pie', 0))
    return tuple([
        Firm(funds=initial_funds) for i in xrange(number)
    ])


################################################################
# Culture Evaluators
def _culture_matrix(firm):
    return [agent.culture for agent in firm.workers]
    
def _culture_cols(firm):
    m = _culture_matrix(firm)
    a = len(firm.workers)
    
    if not firm.workers:
        return []
        
    return [
      [m[row_i][col_i] for row_i in xrange(a)] 
      for col_i in xrange(len(m[0]))
    ]
    
def _culture_vector(firm):
    """return a float vector of column means across agents"""
    l = [
      mean(col)
      for col in _culture_cols(firm)
    ]
    return l

def _quadratic_sameness(c):
    """Map the culture vector so that firms are rewarded for matches of all
    ones or all zeros, but not diversity. (We are pre-supposing some very
    controversial issues about work here- e.g. that group think is never a
    cost and that diversity lends nothing to a firm.)"""
    return 1 - 4 * c * (1-c)

def _tent_sameness(c):
    """Same as a quadratic map, but uses abs, which means that it has the same
    mean value as the identity map under random shuffling"""
    return 2 * abs(c - 0.5)

_culture_transforms = dict(
    tent=_tent_sameness,
    quad=_quadratic_sameness
)

def culture_vector_mapped_mean(firm, params=None):
    """Return the mean culture vector. This is not a great meaure of anything
    unless the vector is non-linearly transformedd, else the overall mean will
    always be the same, no matter how you distribute the rows."""
    if not firm.workers:
        return 0
    transform = _culture_transforms.get(params.culture_transform)
    return sum(map(transform, _culture_vector(firm)))/len(firm.workers)

################################################################
# Knowledge Evaluators
def knowledge_sum(firm, params=None):
    return sum([worker.knowledge for worker in firm.workers])


################################################################
# Competitiveness (Culture-Knowledge interaction) Evaluators
def power_competitiveness(world, params):
    firm_culture_fn = get_callable(
        params.firm_culture_fn)
    firm_knowledge_fn = get_callable(
        params.firm_knowledge_fn)
    
    for firm in world.firms:
        firm.competitiveness = (
            firm_culture_fn(firm, params) ** params.culture_exponent *
            firm_knowledge_fn(firm, params) ** params.knowledge_exponent *
            len(firm.workers)
        )

################################################################
# Return-on-competitiveness Evaluators
def linear_market(firms, params=None):
    """The size of the slice of pie you get is equal to your proportional
    skill."""
    competitivenesses = [firm.competitiveness for firm in firms]
    total_competitiveness = sum(competitivenesses)
    #avoid divide-by-0
    if total_competitiveness<=0.0:
        return [0] * len(competitivenesses)
    return [comp/total_competitiveness for comp in competitivenesses]

def quantal_response_fairy_dust(competitivenesses, params=None):
    raise NotImplementedError


################################################################
# Public return-on-worker Valuators
def assign_worker_k_share_firm_income(world, params=None):
    """Assuming linear return on overall firm knowledge and income effects,
    how much of the firm's income is due to this guy?
    
    We do this per-firm basis, for convenience.
    
    We inject the results rather than returning them because otherwise i might
    mess up dict key stability later.
    """
    for firm in world.firms:
        firm_knowledge = sum([w.knowledge for w in firm.workers])
        for worker in firm.worker_dict:
            worker.canonical_valuation = firm.income * worker.knowledge / \
                firm_knowledge

def lag_valuations(world, params=None):
    for worker in world.workers:
        worker.lagged_valuations.append(worker.canonical_valuation)
        if len(worker.lagged_valuations) > params.worker_valuation_lag:
            worker.lagged_valuations = worker.lagged_valuations[
                -params.worker_valuation_lag:]

################################################################
# stats logging functions
"""
The stats protocol is, given a world state, return a lag key to store stats
in, and a stat.
"""
def track_wages(world):
    return dict([
        (unicode(worker), worker.wage) for worker in world.workers
    ])

def track_incomes(world):
    return dict([
        (unicode(firm), firm.income) for firm in world.firms
    ])
    
def track_expenses(world):
    return dict([
        (unicode(firm), firm.expenses) for firm in world.firms
    ])

def track_profits(world):
    return dict([
        (unicode(firm), firm.profit) for firm in world.firms
    ])
    
def track_funds(world):
    return dict([
        (unicode(firm), firm.funds) for firm in world.firms
    ])

def track_canonical_valuations(world):
    return dict([
        (unicode(worker), worker.canonical_valuation) for worker in world.workers
    ])
    
def track_employment(world):
    employments = []
    for firm in world.firms:
        for worker in firm.workers:
            employments.append((unicode(worker), unicode(firm)))
    return dict(employments)

################################################################
# auction functions
def _massage_bid_order_to_favour_boss(bid_order, firm_list, worker):
    """this function returns a permutation that references the current boss
    last, to favour staying put in the event of equal bids"""
    #operate on a copy:
    l = list(bid_order)
    bid_order.append(bid_order.pop(firm_list.index(worker.firm)))
    return l
    
def _leave_bid_order_alone(bid_order, firm_list, worker):
    return bid_order

def single_random_staff_auction(world):
    """
    All traders look at their valuations of all agents and make random
    bids based on that valuation and the current wage.
    """
    # The flag that says whether agents are required to accept lower-value
    # contracts if there are no higher bidders (i.e. that they re-apply for
    # their own job)
    wage_ratchets = world.params.get('auction_wage_ratchets', False)
    firm_bid_fn = get_callable(world.params['firm_bid_fn'])
    firm_private_worker_valuator = get_callable(world.params['firm_private_worker_valuator'])
    favour_own_boss = world.params.get('favour_own_boss', False)
    
    if favour_own_boss:
        massage_bid_order = _massage_bid_order_to_favour_boss
    else:
        massage_bid_order = _leave_bid_order_alone
        
    # get a gradual permutation of the firms so that no particular firm is a
    # favoured employer for equal bids
    num_firms = len(world.firms)
    bid_order = sample(range(num_firms), num_firms)
    firm_list = world.firms
    
    for worker in world.workers:
        firm_bids = {}
        #rotate the permutation to keep it fresh
        bid_order.append(bid_order.pop(0))
        
        for firm_idx in massage_bid_order(bid_order, firm_list, worker):
            firm = firm_list[firm_idx]
            valuation = firm_private_worker_valuator(firm, worker, world)
            bid = firm_bid_fn(firm, worker, valuation, world)
            firm_bids[firm] = bid
        
        #OK, now we have some fair, if naïve, bids.
        #Let's see which worker ends up where
        
        best_firm, best_wage = None, 0.0
        for firm, wage in firm_bids.iteritems():
            if wage >= best_wage:
                best_firm, best_wage = firm, wage
        #Do any bids exceed current wage?
        if (wage_ratchets and (best_wage > worker.wage)) or \
                not wage_ratchets:
            worker.resign()
            if best_firm is not None:
                worker.join(best_firm,
                    wage=best_wage,
                    valuation=valuation)
            else:
                # If best_firm *is* None, we have an unemployed worker.
                # When does this happen?
                world.engine.message("no winning bids for %s", worker)

################################################################
# bid generating functions
def uniform_bid_in_mutual_profit_range(firm, worker, valuation, world):
    """
    Given their valuation, how much does this firm bid for this worker? A
    random amount up to their valuation, greater, if possible, than the
    current wage, so that both parties may benefit
    """
    #never any sense bidding higher than the worker valuation
    high_bid = valuation
    #no sense bidding lower than the current wage, unless they are overpaid
    low_bid = min(high_bid, worker.wage)
    return low_bid + random() * (high_bid - low_bid)

def uniform_bid_in_firm_profit_range(firm, worker, valuation, world):
    """
    Given their valuation, how much does this firm bid for this worker? A
    random amount up to their valuation.
    """
    return random() * valuation

def uniform_bid_in_firm_profit_range_keeping_good_staff(firm, worker, valuation, world):
    """
    Given their valuation, how much does this firm bid for this worker?
    For non-hired workers, a random amount up to their valuation. But own
    staff get their wage guaranteed.
    """
    if worker not in firm.workers:
        return random() * valuation
    else:
        return worker.wage
    
def bid_valuation(firm, worker, valuation, world):
    """
    given their valuation, how much does this firm bid for this worker?
    The valuation, straight-out.
    """
    return valuation
    
################################################################
# Firm death and birth functions

def market_forces(world):
    pass


################################################################
# firm-centric value estimating functions
def use_canonical_valuation(firm, worker, world):
    return worker.canonical_valuation

def use_lagged_canonical_valuation(firm, worker, world):
    return worker.lagged_valuations[0]

def fast_private_lagged_canonical_valuation(firm, worker, world):
    if worker in firm.workers:
        return worker.canonical_valuation
    # if len(worker.lagged_valuations)>2:
    #     import pdb; pdb.set_trace()()
        
    return worker.lagged_valuations[0]

def use_canonical_valuation_by_flat_risk(firm, worker, world):
    """calculate a (geometric?) mean ratio between predicted and realised
    worker return and use that to evaluate future hires."""
    pass

def use_canonical_valuation_by_pairwise_risk(firm, worker, world):
    """calculate a different (geometric?) mean ratio between predicted and
    realised worker return, depending on whom the worker was hired from, and
    use that to evaluate future hires."""
    ratios = world.worker_tracker['log_firm_ratios']
    current_firm = worker.firm
    if world.timestep>1:
        trace()()
    return exp(ratios.get((worker.firm, firm), 0))

################################################################
# the tools to estimate worker value based on sampling history

def track_worker_productivity_pairwise(world):
    """track the mean value of a worker from step to step based on the
    transfer between one firm and the next. We keep track of number of sample
    points as an estimate of uncertainty, though this is not currently
    used, nor is a variance estimate calculated.
    
    This method assumes that, c.p., effectiveness is linear with knowledge,
    and that the usefulness fo the observations decay exponentially with time
    in some fuzzy fashion that could be made more rigorous."""
    worker_tracker = world.worker_tracker
    worker_last_step = worker_tracker['last_step']
    worker_this_step = {}
    log_firm_ratios = worker_tracker['log_firm_ratios']
    sample_counts = worker_tracker['sample_counts']
    firm_ratio_updates = {}
    ratio_count_updates = {}
    _hot_ratio_action = {}
    #note this doesn't work if the half life is *actually* infinite
    decay_factor = half_life_steps_to_ratio(
        world.params.get('learning_half_life', float('inf'))
    )
    
    for worker in world.workers:
        worker_this_step[worker] = (worker.firm, worker.canonical_valuation)
    
    # if world.timestep>1:
    #     import pdb; pdb.set_trace()()

    for worker in worker_last_step:
        new_firm, new_valuation = worker_this_step[worker]
        old_firm, old_valuation = worker_last_step[worker]
        this_ratio_log = log(new_valuation/old_valuation)
        _ratio_havers = _hot_ratio_action.setdefault(this_ratio_log, [])
        _ratio_havers.append((worker, new_firm, new_valuation, old_firm, old_valuation))
        forward_ratios = firm_ratio_updates.setdefault(
            (old_firm, new_firm), []
        )
        if len(forward_ratios)>0:
            trace()()
            
        backward_ratios = firm_ratio_updates.setdefault(
            (new_firm, old_firm), []
        )
        this_count = ratio_count_updates.get(
            (old_firm, new_firm), 0
        ) + 1
        forward_ratios.append(this_ratio_log)
        backward_ratios.append(-this_ratio_log)
        ratio_count_updates[(old_firm, new_firm)] = this_count
        ratio_count_updates[(new_firm, old_firm)] = this_count

    if world.timestep>1:
        trace()()
    
    all_tuples = set(firm_ratio_updates.keys()).union(
        log_firm_ratios.keys())
        
    for firm_tuple in all_tuples:
        sample_counts[firm_tuple] = sample_counts.get(firm_tuple, 0) * decay_factor + (1 - decay_factor) * ratio_count_updates.get(firm_tuple, 0)
        log_firm_ratios[firm_tuple] = log_firm_ratios.get(firm_tuple, 0) * (decay_factor) + (1-decay_factor) * mean(firm_ratio_updates.get(firm_tuple , [0]))
    
    worker_tracker['last_step'] = worker_this_step

def _track_worker_productivity_pairwise_setup(world):
    world.worker_tracker = {
        'last_step': {},
        'log_firm_ratios': {},
        'sample_counts': {},
    }

track_worker_productivity_pairwise.setup = _track_worker_productivity_pairwise_setup