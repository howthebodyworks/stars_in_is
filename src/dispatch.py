#!/usr/bin/env python
# encoding: utf-8
"""
dispatch.py

Created by dan mackinlay on 2012-01-04.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.

Trial dispatchers. sim.main is the presumed thing to dispatch. This can either be reached as a native funcion, a la multiprocessing/python futures, or by invoking the CLI
"""

import sim
from storage import ExperimentStorage, digest
from lib.util import pickle, makedirs
import sys
from settings import PARAM_PATH, OUTPUT_PATH
import os

class Dispatcher(object):
    """Base dispatcher does in-process handling. no concurrency"""
    def __init__(self, exp_params, *args, **kwargs):
        super(Dispatcher, self).__init__(*args, **kwargs)
        self.pending = {}
        self.storage = ExperimentStorage(exp_params['experiment_id'])
    
    def dispatch(self, trial_params,
            verbosity=0,
            *args, **kwarg):
        """handle one trial sim"""
                
        key = self.storage.meta_as_key(trial_params)
        self.pending[key] = None
        return write_to_file(
            storage=self.storage,
            trial_params=trial_params,
            verbosity=verbosity,
            key=key)
        
    def wait(self):
        """if this object had concurrency-handling, this method would wait
        until all sims had returned"""
        return
    
    def pop_all_finished(self):
        """Easy to pop the finished ones here, since they are all always
        finished"""
        pending = self.pending
        while len(pending)>0:
            key = pending.iterkeys().next()
            self.pop(key)
            yield key
    
    def pop(self, key):
        self.pending.pop(key)

class LocalCLIDispatcher(Dispatcher):
    """invoke the script from the command line. This is OpenPBS-happy."""
    def __init__(self, *args, **kwargs):
        super(LocalCLIDispatcher, self).__init__(*args, **kwargs)
        import subprocess
        self.subprocess = subprocess
        self.python = sys.executable
        self.pending_args = {}
        self.pending_output = {}
        self.pending_procs = {}
        
    def dispatch(self, trial_params,
            verbosity=0,
            *args, **kwargs):
        subprocess = self.subprocess
        key = self.storage.meta_as_key(trial_params)
        argpath = os.path.join(PARAM_PATH, key)
        makedirs(PARAM_PATH)
        with open(argpath, 'wb') as handle:
            pickle.dump(trial_params, handle, protocol=2)
        outputpath = os.path.join(OUTPUT_PATH, key)
        makedirs(OUTPUT_PATH)
        proc_args = [
            self.python, './do_trial.py',
            '--arg-file', argpath,
            '--verbosity', verbosity,
            '--key', key
        ]
        with open(outputpath, 'wb') as handle:
            proc = subprocess.Popen(
                proc_args,
                stdout=handle,
                stderr=subprocess.STDOUT
            )
            self.pending_args[key] = trial_params
            self.pending_output[key] = handle
            self.pending_procs[key] = proc
            self.pending[key] = None
            return key
    
    def wait(self, poll_interval=10):
        from time import sleep
        while not all(
            [(proc.returncode is not None)
            for proc in self.pending_procs.itervalues()]):
            for proc in self.pending_procs.itervalues():
                proc.poll()
            sleep(poll_interval)
    
    def pop(self, key):
        self.pending.pop(key)
        self.pending_args.pop(key)
        self.pending_output.pop(key)
        self.pending_procs.pop(key)
    
    def pop_all_finished(self):
        pending = self.pending
        finished_keys = [
            key for key in pending
            if self.pending_procs[key].returncode is None]
        for key in finished_keys:
            self.pop(key)
            yield key

def write_to_file(storage,
        trial_params,
        verbosity=0,
        key=None):
    trial = sim.main(params=trial_params, verbosity=verbosity)
    key = storage.put(trial, trial_params, key)
    return key
