#!/usr/bin/env python
# encoding: utf-8
"""
workers.py

The classes that hold the minimal state necessary for this model.

Was gonna make it all named tuples, but empty members are nice.

factory params protocol is as per the firm_arithmetic module

Created by dan mackinlay on 2011-09-11.
"""
from __future__ import division
from random import randrange, random, sample
from lib.bunch import Bunch

class Agent(object):
    """abstract superclass Agent provides stable hashing logic"""
    _displayable_attrs = ['_id']
    
    def __init__(self, _id=None, *args, **kwargs):
        """We allow stipulating an id for ease of resurrecting"""
        if _id is None:
            #No _id specified - generate from class
            _id =  getattr(self.__class__, '_id_counter', 0) + 1
            self.__class__._id_counter = _id
        else:
            # id specified - update class accordingly.
            self._update_class_counter(_id)
        self._id = _id
    
    def _update_class_counter(self, _id):
        self.__class__._id_counter = max(
            getattr(self.__class__, '_id_counter', 0),
            _id
        )
    def __unicode__(self):
        return u"<%s:%d>    " % (self.__class__.__name__, self._id)

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__,
          ','.join([
            ("%s=%s" % (name, repr(getattr(self,name))))
            for name in self._displayable_attrs
          ]))
    
    def __setstate__(self, state):
        self.__dict__ = state
        self._update_class_counter(state['_id'])

class Worker(Agent):
    """Workers correspond to individuals.
    """
    _displayable_attrs = ['_id', 'culture', 'knowledge', 'wage', 'canonical_valuation']
    
    def __init__(self,
            culture,
            knowledge=None,
            wage=0.0, 
            canonical_valuation=0.0,
            firm=None,
            params=None,
            *args, **kwargs):
        self.culture = culture
        self.knowledge = knowledge or random()
        self.wage = wage
        self._firm = firm
        self.canonical_valuation = canonical_valuation
        self.lagged_valuations = [0]
        super(Worker, self).__init__(*args, **kwargs)
    
    def resign(self, *args, **kwargs):
        if self._firm is not None:
            old_firm = self._firm
            self._firm = None
            old_firm._remove_worker(self)
        self.wage = 0.0
            
    def join(self, firm, wage=None, *args, **kwargs):
        """sacking and hiring relationship management happens from the worker
        end, since workers don't necessarily have firms"""
        self._firm = firm
        #wage should really be a relational property.
        if wage is not None:
            self.wage = wage
        firm._add_worker(self, *args, **kwargs)
    
    def get_firm(self):
        return self._firm
    firm = property(get_firm)
    
class Firm(Agent):
    """A Firm's funds are its capital. Its competitiveness is how good it is,
    but its income is how good its returns are after market effects are
    taken into account"""
    
    _displayable_attrs = ['_id', 'funds', 'income', 'competitiveness']
    
    def __init__(self,
            funds=0.0,
            income=0.0,
            competitiveness=0.0,
            workers=None,
            params=None,
            *args, **kwargs):
        self.funds = funds
        self.income = income
        self.expenses = 0
        self.profit = 0
        self.competitiveness = competitiveness
        self.worker_dict = {}
        for worker in (workers or []):
            worker.join(self)
        super(Firm, self).__init__(*args, **kwargs)
        
    def _add_worker(self, worker, valuation=None):
        # worker_dict content, for now, is a Bunch holding a value estimate by
        # the firm. Other stuff could be stashed there.
        worker_meta = Bunch(valuation=valuation)
        if valuation is not None:
            worker_meta.valuation = valuation
        self.worker_dict[worker] = worker_meta
    
    def _remove_worker(self, worker):
        del(self.worker_dict[worker])
        
    def get_workers(self):
        return self.worker_dict.keys()
    workers = property(get_workers)
    
    def do_accounts(self, income=0):
        """
        All firm internal book-keeping happens here. it's assumed to be called
        after all market profit distribution and so on, and the firm's market
        income allocation is passed in.
        """
        expenses = sum([w.wage for w in self.workers])
        profit = income - expenses
        self.funds += profit
        self.profit = profit
        self.expenses = expenses
        self.income = income
