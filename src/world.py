#!/usr/bin/env python
# encoding: utf-8
"""
world.py

Created by dan mackinlay on 2011-11-24.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""
from __future__ import division
from lib.dynamic import get_callable
from logs import Log
from settings import SKIP_STEPS
from engine import Engine

class World(object):
    """Holds simulation state so that statistics may be gathered against it"""
    def __init__(self,
            params, engine=None):
        self.timestep = 0
        self.log = Log()
        self.firms = []
        self.workers = []
        self.timestep = 0
        self.params = params
        #and, to save typing...
        self.stats = params.stats
        if engine:
            self.engine = engine
        else:
            self.engine = Engine()
        
    def __iter__(self, num_steps=None):
        """
        Invoking the world iterator causes this guys to yield numbered
        timesteps and collect stats.
        """
        start = self.timestep
        stop = self.timestep + (num_steps or self.params.num_steps)
        for i in xrange(start, stop):
            self.timestep = i
            yield i
            if self.timestep>SKIP_STEPS:
                self.gather_stats()
    
    def add_workers(self, worker_iterable):
        self.workers.extend(worker_iterable)
    
    def add_firms(self, firm_iterable):
        self.firms.extend(firm_iterable)
        
    def steps(num_steps = 1):
        """convenient method for doing small numbers of steps"""
        for ret in self.__iter__(num_steps): yield ret
        
    def gather_stats(self):
        """this appends a statistic for every timestep to a log. I wonder if I
        should instead add a step number and not require every step have a
        value?"""
        for stat_fn_name in self.stats:
            stat_fn = get_callable(stat_fn_name)
            self.log.log_dict(stat_fn_name, stat_fn(self))
    
    def __getstate__(self):
        state = self.__dict__.copy()
        state.pop('engine')
        return state

