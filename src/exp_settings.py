#The default set that all others are defined relative to. This bundle is
#special, in that all others get merged with it
DEFAULT_PARAMS = dict(
    num_steps=150,
    num_firms=10,
    num_workers=100,
    culture_length=10,
    culture_density=0.5,
    culture_transform='tent',
    culture_exponent=1,
    auction_wage_ratchets=False,
    knowledge_exponent=1,
    firm_culture_fn='fns.culture_vector_mapped_mean',
    firm_knowledge_fn='fns.knowledge_sum',
    firm_competitiveness_fn='fns.power_competitiveness',
    firm_returns_fn='fns.linear_market',
    firm_bid_fn='fns.uniform_bid_in_firm_profit_range',
    favour_own_boss=False,
    firm_initial_funds_pie=1,
    worker_canonical_valuator='fns.assign_worker_k_share_firm_income',
    worker_canonical_valuation_lagger='fns.lag_valuations',
    worker_valuation_lag=0,
    firm_private_worker_valuator='fns.use_canonical_valuation',
    worker_factory='fns.random_workers',
    firm_factory='fns.random_firms',
    hiring_fn='fns.single_random_staff_auction',
    firm_info_updater='fns.null',
    oversampling=5,
    stats=(
        'fns.track_wages',
        'fns.track_canonical_valuations',
        'fns.track_incomes',
        'fns.track_profits',
        'fns.track_expenses',
        'fns.track_funds',
        'fns.track_employment'
    ),
)

safe_workers = dict(
    wage_ratchets=True
)

safeish_workers = dict(
    wage_ratchets=False,
    firm_bid_fn='fns.uniform_bid_in_firm_profit_range_keeping_good_staff',
    favour_own_boss=True,
)

default = {}

laggy_asymmetry = dict(
    worker_valuation_lag=11,
    firm_private_worker_valuator='fns.fast_private_lagged_canonical_valuation',
)

weight_risk_flat = dict(
    firm_private_worker_valuator='fns.use_canonical_valuation_by_flat_risk',
    firm_info_updater='fns.track_worker_productivity_pairwise',
    learning_half_life=10,
)

honest_bid = dict(
    firm_bid_fn='fns.bid_valuation',
)
