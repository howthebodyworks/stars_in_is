#!/usr/bin/env python
# encoding: utf-8
"""
A central control room wherefrom all commands are launched - not out of logical 
necessity, but for semantic clarity, and ease of data tracking with, e.g. 
sumatra.
"""
import argparse
# from settings import *
import sys
import dispatch

def parser_factory():
    parser = argparse.ArgumentParser(
      prog="do_experiment",
      description="Run some simulation experiment over a range of values")
    parser.add_argument('experiment_name',
      type=str,
      default='default',
      help='experiment name. see exp_settings.py for permissable values')
    parser.add_argument('--experiment-id',
      type=str,
      default=None,
      help='experiment id, for finding your results later. Defaults to experiment name.')
    parser.add_argument('--seed-offset',
      type=int,
      help="seed for the first trial. subsequent seeds are generated in"
      " linear progression.",
      default=0)
    parser.add_argument('--verbosity',
      type=int,
      help="How prolix must i be?",
      default=0)
    parser.add_argument('--concurrency',
      type=str,
      help="Concurrency model",
      default='Dispatcher',
      choices=['Dispatcher', 'LocalCLIDispatcher'])
    parser.add_argument('--params', type=str, default='',
    help="override a sim param from the command line")
    # parser.add_argument('--processes', type=int, default=1,
    #   help='how many CPUs to use')
    parser.add_argument('--interactive', action='store_true',
      default=False, help="open a shell upon completion")

    return parser

def main(str_vers=None, *cl_args):
    """gets our sophisticated parser and work out the invocation args"""
    
    parser = parser_factory()
    
    #but first we allow non CLI invocation as well, for ease of profiling,
    #by allowing us to override CLI args
    if str_vers:
        #the first arg, if it exists, is parsed as with a CLI string
        import shlex
        str_vers = shlex.split(str_vers)
        str_vers.extend(cl_args)
        cl_args = str_vers
        
    if not cl_args:
        cl_args = sys.argv[1:]
    
    args = parser.parse_args(cl_args)
    arg_string = ' '.join(cl_args)

    #now do some legwork to dispatch the arguments to the appropriate function
    args_dict = dict(args._get_kwargs())
    args_dict['command_line'] = arg_string

    return simulate_experiment(**args_dict)

def simulate_experiment(experiment_name,
        interactive,
        concurrency='Dispatcher',
        seed_offset=0,
        verbosity=0,
        params='',
        *args, **kwargs):
    import exp_settings
    import dispatch
    from lib.dynamic import get_callable
    
    # support lotsa calling conventions by merging params arg and kwargs
    # then merging with default args. A bit messy.
    exp_bunch = exp_settings.DEFAULT_PARAMS.copy()
    exp_bunch['experiment_name'] = experiment_name
    exp_bunch.update(getattr(exp_settings, experiment_name))
    exp_bunch.update(kwargs)
    
    # do this with exec to parse python from the CLI
    # obviously this is hopelesssly insecure
    exec('extra_params = dict(%s)' % params)
    exp_bunch.update(extra_params)
    
    #Only pass non-null parameters; else let it choose its own default
    for k, v in kwargs.items():
        if v is None:
            del(kwargs[k])
                
    exp_bunch['experiment_id'] = exp_bunch['experiment_id'] or experiment_name
    experiment_id = exp_bunch['experiment_id']

    oversampling = exp_bunch.pop('oversampling')
    
    #Do concurrency magic here.
    dispatcher = getattr(dispatch, concurrency)(exp_params=exp_bunch)
    
    #Do param sweeps here
    for seed in xrange(seed_offset, seed_offset + oversampling):
        exp_bunch['seed'] = seed
        trial_key = dispatcher.dispatch(
            trial_params=exp_bunch,
            verbosity=verbosity)
    dispatcher.wait()
    keys = [key for key in dispatcher.pop_all_finished()]
    for key in keys:
        print key, "finished"
                
    if interactive:
        embedded_shell()()
    
    return keys

if __name__ == "__main__":
   main()


