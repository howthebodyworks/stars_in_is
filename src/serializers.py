#!/usr/bin/env python
# encoding: utf-8
"""
serializers.py

Created by dan mackinlay on 2011-11-25.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""
from path import path
import csv
from logs import Log

def write_trial_log_csv_files(log_dict, dir_path):
    """take a dict of arrays of dicts, and write them to CSV files at a given
    directory"""
    #for convenience, accept log objects OR their data dicts
    if isinstance(log_dict, Log):
        log_dict = log_dict._data
    
    #make into a convenient path object
    dir_path = path(dir_path)
    #...which we can assume exists
    dir_path.makedirs()
    
    for data_key, data_values in log_dict.iteritems():
        write_data_array_csv_file(
            data_values,
            (dir_path/data_key) + '.csv'
        )
    return dir_path

def write_data_array_csv_file(data_values, text_path):
    """Here we have a list of dicts, each of which is presumed to have the
    same keys, which are column headings"""
    text_path = path(text_path)
    header = ['i']
    header.extend([str(h) for h in data_values[0].keys()])
    with text_path.open('wb') as text_file:
        text_writer = csv.DictWriter(text_file,
            fieldnames = header,
            dialect='excel-tab')
        text_writer.writeheader()
        for i, data_dict in enumerate(data_values):
            text_writer.writerow(
                dict(
                    [(str(k), v) for k, v in data_dict.iteritems()],
                    i=i
                )
            )
    return text_path

def write_workers_to_csv_file(workers, text_path):
    """Here we have a list of workers, each of which is presumed to have the
    same keys, which are column headings"""
    text_path = path(text_path)
    header = ['i', 'id', 'knowledge', 'culture']
    with text_path.open('wb') as text_file:
        text_writer = csv.DictWriter(text_file,
            fieldnames = header,
            dialect='excel-tab')
        text_writer.writeheader()
        for i, worker in enumerate(workers):
            text_writer.writerow(
                dict(
                    knowledge=worker.knowledge,
                    culture=repr(worker.culture),
                    i=i,
                    id=unicode(worker)
                )
            )
    return text_path
