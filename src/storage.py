"""
Helpers to store a whole bunch of experiments as pickles
"""
import settings
from lib.fsdict import FSPickleDict
from lib.util import digest
from gzip import GzipFile
import os

def _get_storage_dict(*subdirs):
    result_path = os.path.join(*([settings.EXP_PATH] + list(subdirs)))
    try:
        os.makedirs(result_path)
    except OSError, e:
        # no problem if it already exists
        if e.errno==17:
            pass
        else:
            raise e
    results_dict = FSPickleDict(result_path, handle_factory=GzipFile)
    return results_dict

class ExperimentStorage(object):
    """docstring for ExperimentStorage"""
    def __init__(self, experiment_name, *args, **kwargs):
        super(ExperimentStorage, self).__init__(*args, **kwargs)
        self.experiment_name = experiment_name
        self.datastorage = _get_storage_dict(*[experiment_name, 'data'])
        self.metadatastorage = _get_storage_dict(*[experiment_name, 'index'])
    
    def put(self, trial_data, trial_metadata, key=None):
        """
        You need at least the value and the metadata for the value, i.e. at
        least the *seed* number, and preferably all exp parameters.
        """
        if key is None:
            key = digest(trial_metadata)
        
        self.datastorage[key] = trial_data
        self.metadatastorage[key] = trial_metadata
        
        return key
        
    def meta_as_data_path(self, trial_metadata):
        return self.key_as_path(self.meta_as_key(trial_metadata))
    
    def key_as_data_path(self, key):
        return os.path.path.join(self.datastorage.path, key)
    
    def meta_as_key(self, trial_metadata):
        return digest(trial_metadata)
        
    def get(self, key=None):
        """find by key.
        
        For convenience, if the key is omitted, we choose an arbitrary
        item."""
        if key is None:
            key = self.keys()[0]
        return ExperimentTrial(
            data=self.datastorage[key],
            metadata=self.metadatastorage[key],
            key=key,
            storage=self)
    
    def keys(self):
        return self.metadatastorage.keys()

class ExperimentTrial(object):
    """glorified namedtuple"""
    def __init__(self, data, metadata, key, storage=None, *args, **kwargs):
        super(ExperimentTrial, self).__init__(*args, **kwargs)
        self.data = data
        self.metadata = metadata
        self.storage = storage
        