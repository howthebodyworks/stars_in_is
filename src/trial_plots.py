from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from pandas import DataFrame, merge
from scipy import stats, mgrid, c_, reshape, random, rot90
from lib.util import trace
import re
from lib.calcs import double_affine_cipher

_parse_rep_re = re.compile(u'\<(\w+)\:(\d+)\>')
def _parse_rep(rep):
    "Parse a <Worker:id> string into (klass, id)"
    klass, _id = _parse_rep_re.match(rep).groups()
    return klass, int(_id)

# data preening functions:
def _delta_valuations(world):
    data = world.log._data
    keys = world.log._data.keys()
    valuations = DataFrame(data['fns.track_canonical_valuations'])
    return valuations.diff()[1:]

def _worker_attributes(world):
    workers = world.workers
    return DataFrame([
        {
            'id': unicode(worker),
            'knowledge': worker.knowledge 
        }
        for worker in workers
    ])

def _hash_to_colour(natural_n):
    "convert a number to a colour in a stable fashion"
    colour = double_affine_cipher(natural_n) >> 8
    return "#%06x" % colour

def _series_to_observations(dataframe):
    """Convert a DataFrame of TimeSeries columns into a melted array.
    I feel there should be a way to do this without going via an intermediate
    list-of-dicts."""
    return DataFrame(list(
        {'step': step, 'id': colname, 'value': value}
        for (step, colname, value) in _col_series_iterator(dataframe)
    ))
    
def _col_series_iterator(dataframe):
    for colname in dataframe.columns.values:
        series = dataframe[colname]
        for step in series.index:
            yield step, colname, series[step]

def _kernel_density_regression(
        xdata, ydata, ax,
        xmin=None, ymin=None,
        xmax=None, ymax=None,
        xlabel='', ylabel=''):
    """plot a kernel density regression. scipy's functionality here is not
    great, not supporting, e.g. bandwidth per-axis, or cross-validated
    bandwidth selection. Pfft.
    
    Also the API is a little gross, necessitating much array creation,
    rotation and transposition, which should be hidden away in this private
    function."""
    xmin = xmin or xdata.min()
    xmax = xmax or xdata.max()
    ymin = ymin or ydata.min()
    ymax = ymax or ydata.max()
    X, Y = mgrid[xmin:xmax:100j, ymin:ymax:100j]
    # plt.autoscale(False)
    # ax.set_aspect(10)
    positions = c_[X.ravel(), Y.ravel()]
    values = c_[xdata, ydata]
    kernel_fit = stats.kde.gaussian_kde(values.T)
    Z = reshape(kernel_fit(positions.T).T, X.T.shape)
    plt.imshow(
        rot90(Z),
        cmap=plt.cm.gist_earth_r,
        extent=[xmin, xmax, ymin, ymax],
        aspect='auto'
    )
    plt.plot(
        xdata,
        ydata,
        'k.', markersize=2
    )
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.draw()
    #import IPython; IPython.embed()
    return ax

#actual graphs
def disappointment_histogram(world, ax=None):
    if ax is None:
        fig = plt.figure()
        ax = plt.gca()
    else:
        fig = plt.gcf()
    # ax = fig.add_subplot(2,1,1) # two rows, one column, first plot
    delta_valuations = _delta_valuations(world)
    all_deltas = np.asarray(delta_valuations).flatten()
    plt.hist(all_deltas, bins=30)
    # plt.ion()
    plt.draw()
    return ax

def disappointment_versus_knowledge(world, ax=None):
    #Divide worker disappointment by knowledge
    return ax
    
def disappointment_versus_wage(world, ax=None):
    return ax
    
def disappointment_versus_knowledge(world, ax=None):
    return ax

def staff_movement_chart(world, ax=None, step_limit=None):
    if ax is None:
        fig = plt.figure()
        ax = plt.gca()
    else:
        fig = plt.gcf()
    employment_strings = world.log._data['fns.track_employment']
    if step_limit is None:
        step_limit = len(employment_strings[employment_strings.keys()[0]])
    employment_nums = {}
    all_firms = set()
    for worker_string, firm_string_list in employment_strings.iteritems():
        worker_id = _parse_rep(worker_string)[1]
        worker_firms = [
            _parse_rep(firm_string)[1] for firm_string in firm_string_list
        ]
        all_firms = all_firms.union(worker_firms)
        employment_nums[worker_id] = worker_firms
    del(employment_strings)
    num_workers = len(employment_nums)
    for worker_id in employment_nums:
        employment_nums[worker_id] = [
            firm + 0.5 * worker_id / num_workers - 0.25
            for firm in employment_nums[worker_id]
        ]
    step_numbers = range(step_limit)
    for worker_id in employment_nums:
        plt.plot(
            step_numbers,
            employment_nums[worker_id][:step_limit],
            color=_hash_to_colour(worker_id)
        )
    for firm_id in all_firms:
        plt.plot(
            step_numbers,
            [firm_id] * step_limit,
            color='black',
            marker='o',
            linewidth=2,
        )
    plt.xlabel('time')
    plt.ylabel('firm')


def wage_versus_previous_wage(world, ax=None):
    if ax is None:
        fig = plt.figure()
        ax = plt.gca()
    else:
        fig = plt.gcf()

    wages = np.asarray(DataFrame(world.log._data['fns.track_wages']))
    x = wages[0:-1,:].flatten()
    y = wages[1:,:].flatten()
    _kernel_density_regression(
        np.log(x), np.log(y),
        ax=ax,
        xlabel="log(previous wage)",
        ylabel="log(current wage)"
    )
    return ax
    
def wage_versus_previous_wage(world, ax=None):
    if ax is None:
        fig = plt.figure()
        ax = plt.gca()
    else:
        fig = plt.gcf()

    wages = np.asarray(DataFrame(world.log._data['fns.track_wages']))
    x = wages[0:-1,:].flatten()
    y = wages[1:,:].flatten()
    _kernel_density_regression(
        np.log(x), np.log(y),
        ax=ax,
        xlabel="log(previous wage)",
        ylabel="log(current wage)"
    )
    return ax
    
def wage_versus_knowledge(world, ax=None):
    """
    regress worker wage against knowledge
    """
    if ax is None:
        fig = plt.figure()
        ax = plt.gca()
    else:
        fig = plt.gcf()
    
    wages = DataFrame(world.log._data['fns.track_wages'])
    flat_wages = _series_to_observations(wages)
    worker_data = _worker_attributes(world)
    knowledge_vs_wage = merge(flat_wages, worker_data, 'left', on=['id'])
    _kernel_density_regression(
        knowledge_vs_wage['knowledge'],
        np.log(knowledge_vs_wage['value']),
        ax=ax,
        xmin=0, xmax=1,
        xlabel="knowledge",
        ylabel="logwage"
    )
    #import IPython; IPython.embed()
    return ax