#!/usr/bin/env python
# encoding: utf-8
"""
sampler.py

Created by dan mackinlay on 2011-12-08.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.

A generic parameter sample kit.

Take a default parameter bunch, permute various parameters, return a hash to
identify it.
"""

class Sampler(object):
    """I sample parameter ranges"""
    def __init__(self, params, dispatcher, *args, **kwargs):
        super(Sampler, self).__init__(*args, **kwargs)
        self.params = params
        self.dispatcher = dispatcher
    
    def go(self):
        
    
        