#!/usr/bin/env python
# encoding: utf-8
"""
logs.py

Created by dan mackinlay on 2011-11-25.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""
from collections import defaultdict

class Log(object):
    def __init__(self, *args, **kwargs):
        self._data = {}
    
    def log_dict(self, log_key, log_dict):
        """for now all values are dicts, so we can make some simplifying
        assumptions about how to serialise them."""
        sub_log = self._data.setdefault(log_key, {})
        for key, val in log_dict.iteritems():
            sub_series = sub_log.setdefault(key, [])
            sub_series.append(log_dict[key])

