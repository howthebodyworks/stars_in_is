#!/usr/bin/env python
# encoding: utf-8
"""
engine.py

Created by dan mackinlay on 2012-01-16.
Copyright (c) 2012 __MyCompanyName__. All rights reserved.
"""

from pprint import pformat

def _console_print(*args):
    for a in args:
        print a

class Engine(object):
    """encapsulates communicating with the outside world across different
    process models"""
    def __init__(self, verbosity=0, *args, **kwargs):
        super(Engine, self).__init__(*args, **kwargs)
        self.verbosity = verbosity
        if verbosity>0:
            self.message = _console_print
        else:
            self.message = lambda *args: None
        
    def pmessage(self, *args):
        for a in args:
            self.message(pformat(a))
