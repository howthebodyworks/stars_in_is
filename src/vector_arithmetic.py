#!/usr/bin/env python
# encoding: utf-8
"""
vector_arithmetic.py

Created by dan mackinlay on 2011-11-18.
Copyright (c) 2011 __MyCompanyName__. All rights reserved.
"""
from __future__ import division
from random import randrange, random, sample
import math

def rand_bool_vector_comb(size, n_trues):
    """A boolean vector of size ``size'' with a known number of true bits,
    distributed randomly
    """
    vector = [0] * size
    true_idxs = sample(range(size), n_trues)
    for idx in true_idxs:
        vector[idx] = 1
    return tuple(vector)

def rand_bool_vector(size, truth_density):
    """A boolean vector of ``size'' coin-tosses with given probability
    """
    return tuple([int(random()<truth_density) for i in xrange(size)])

def mean(vec):
    """return the mean of a vector"""
    return sum(vec)/len(vec)

def log_list(vec):
    """broadcast logarithm to a list, np.ufunc style"""
    return [math.log(e) for e in vec]

def geom_mean(vec):
    """geometric mean of a list"""
    return math.exp(mean(log_list(vec)))

