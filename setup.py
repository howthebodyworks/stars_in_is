import os
from setuptools import setup

# Utility function to read the README file.  
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "stars_in_is",
    version = "0.0.5",
    author = "Dan MacKinlay",
    author_email = "fillmewithspam@email.possumpalace.org",
    description = ("Crazy agent-based schemes."),
    license = "BSD",
    keywords = "example documentation tutorial",
    url = "http://packages.python.org/stars_in_is",
    packages=['stars_in_is', 'stars_in_is.lib', 'tests'],
    package_dir = {'stars_in_is': 'src'},
    long_description=read('README'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
