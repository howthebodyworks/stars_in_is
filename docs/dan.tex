\documentclass[11pt]{amsart}
\usepackage{geometry} % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper} % ... or a4paper or a5paper or ... 
\usepackage[parfill]{parskip}
% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\begin{document}

\title{Groysberg traders}
\author{Dan MacKinlay and Terry Bossomaier}
\maketitle


Each worker $w$, indexed by $j$, has two fixed parameters: a knowledge value, $b_j$;
and a culture vector $c_j$.
Knowledge is presumed public,
but the culture vector is visible to other agents only by its effects on productivity.

$c_j$ is binary, of length, say, $8$.
$b_j$ is scalar, chosen uniformly from the interval $[0,1]$,
and the elements of $c_k$ are chosen by i.i.d. fair coin-toss from $\{0,1\}$.

There is a fixed finite market whose per-turn returns are fixed at unity.

Each firm, $F_k$ is associated with a set $W_k(t)$ of workers at each timestep $t$,
which partition the workforce, i.e. each worker has at most one employer.
Each firm pays each worker a wage $\omega_j(t)$, determined by means that will be
expanded momentarily. 
We denote by $f(n, t)$ the index of the firm whose
worker set contains $w_n$ at time $t$.
(We sometimes omit the $t$ parameter in this function, and the $t$ parameters in series variables,
when the timestep may be assumed from context.)

Workers are initially assigned to firms at random with zero wages.

Each turn, each firm receives a pro-rata distribution of the market, $R_k(t)$
based on firm effectiveness $E_k(t)$,
\footnote{
Alternately, a formal approach would be to use some game theory model, such as
the Logit Quantal Response Equilibrium, in a simplified form

\[R_k = \frac{e^{\beta E_k}}{\sum_m e^{\beta E_m}}\]

where $R_k$ is the probability that a buyer chooses firm $k$.
I don't do this
at the moment since I have no reason to suppose that the the addition of a new free parameter is merited.
}

\[R_k = \frac{E_k}{\sum_m E_m}\]

Firm profit, $P_k(t)$ is given \[P_k(t)=R_k(t)-\sum_{j \in W_k} \omega_j(t)\] - that is,
the profitability is income minus expenses, where expenses are assumed to be
comprised only of worker wages.

Firm funds, $\Phi_k$, are given
\[\Phi_k(t) = \sum_{i=1}^t P_k(t) + \Phi_k(0)\] for $\Phi_k(0)$ the startup capital.

When a firm has no funds it may not spend any money.
It could optionally be replaced by a new firm with different hiring rules.
This is not yet implemented.

Now we stipulate the performance of firms.
Let $n_k=|W_k|$, i.e. the number of agents employed by firm $F_k$.

We calculate the performance of a firm $k$ by
\footnote{
or if you wanted, you might raise these factors to an arbitrary power because you have
reason to suppose as a kind of nonlinear scaling.

\[R_k=(B_k)^g(C_k)^h n_k\]

You could even enforce
\[g+h=1\]
and make this a weighted geometric mean, if you'd like. Since $B_k$ and $C_k$
have both been specified to have mean between 0 and 1, this would allow a pleasing
symmetry.

For now, I presume, to the contrary, that $g = h = 1$, as it simplifies
creating estimates of the marginal benefit of a given agent with a given
knowledge factor.
}
\[E_k=B_kC_kn_k\]

We need to define the culture multiplier $C_i^k$.
We associate a matrix $\kappa^k$ with each firm at each time step,
comprised of the stacked (horizontal) culture vectors of the agents
employed at that firm.
Then we calculate a culture factor from the column means. 

\begin{align*}
C_k(t) &= 0, & n_k=0 \\
C_k(t) &= \sum_y d\left( \frac{1}{n_k}\sum_{y=1}^{n_k} \kappa^y_i \right), & n_k>0 
\end{align*}

for $y$ ranging over all the elements of the culture vector, and for a
``matching'' reward function $d$ such as\footnote{
Also fun might be:
\[d(x) := \frac{1+\cos\pi x}{2}\]
Later, asymmetric functions might be worth considering.
}

\[d(x) := 2 |x - 1/2|\]

We give the knowledge multiplier more simply as the mean of the vector of all
employee knowledge values.

\[B_k=\sum_{j=1}^{n_k} b_j\]

Workers are assigned, each turn, a ``canonical'' performance measure
$p_j(t)$, based on a share of their firm's market distribution, attributed
pro-rata according to $b_j$.
This is a measure, discarding the effect of
culture vector, and making strong linearisation assumptions, of the marginal
return to a firm of the given worker.

\[p_k(t) = \frac{b_j(t)}{\sum_{j \in W_{f(j)}} b^j(t)}\]

They are also assigned a``published'' valuation, which, in this simplest
version of the sim, is assumed to be the same, but which could be made
different to introduce information asymmetry.
\footnote{
This could also be attributed by Logit Quantal Response Equilibrium if we felt
like maintaining that symmetry between worker value imputation and firm
return allocation, though I see no particular justification for it here.
}

Wages are set by single-side auction with firms as bidders.
At each timestep,
firms estimate the value of each worker.
There are several schemes for this,
but one is simply assuming the canonical valuation is accurate.

Firms make bids for each worker each round randomly distributed in the
interval between their valuation of the worker, and the worker's current wage,
bounded above by the requirement of predicted profitability.
Workers may optionally presumed to have tenure, and only leave for a better wage rather
than having their own contract re-evaluated.
We denote the valuation function $v$ for the value to $F_k$ of worker $w_j$ for the next timestep at timestep
$t$ by
\[v(F_k, w_j, t)\]

This notation implies that all firms share a valuation function, based
presumably on public information.
This assumption can be relaxed.

Now, this makes the problem of whom to hire and at what wage, in general, a
complex game with the potential for nontrivial strategy from both firms and
workers.
But since we presume no knowledge of this underlying model we can
hopefully ignore that.
In general, if the only information source is actual
hiring then over plausible lifetimes for the experiment (real human workers 
not often having more than on the order of a dozen jobs in the same industry)
then we can presume that there are not enough data points to gain true
knowledge of the form of the culture vector plus the value of all worker's
vectors, but that instead some kind of imperfect approximation is used.

So, if we wish to revise the firm strategy when can do this by giving them,
say, risk aversion, or a model of worker value whose parameters they estimate.

I have partially implemented the latter.

The model in this case is that value of a worker $w_j$ to an arbitrary firm
$f_k$ at time $t$ is the canonical value they have at their current firm
$f(j,t)$ multiplied by a coefficient, reflecting the typical change in
canonical values experienced by workers moving from that previous firm to this
one over a timestep.
That is, this valuation function imputes a mean change
coefficient to all pairwise changes in employment.

Symbolically we write
\[v(f_k, w_j, t) := V_{f(j,t),F_k} p_k(t)\]

where $V_{x,y}$ is a matrix of these pairwise firm coefficients.

The estimator for the matrix entries is, for the minute, simply a decaying mean of datapoints
so far, i.e. a normalised 1-pole lowpass filter.
We maintain a running estimate of $V_{x,y}$ based on earlier
transitions.

So, define

\[S_ab(t):= \{t:f(j,t-1)=F_a \land f(j, t)=F_b\}\]

Then we may construct the estimator, $\bar{V}_{x,y}$.

But it leads to a nasty notation explosion.
I might need to (re)define some terms to keep this under control.
Will come back to that.

In any case, I'm still nutting out the precise form for it.
Basically, if the employment at firms over time remains similar, the difference in culture
matrices and hence value of given workers between them might be sufficiently
well approximated by this to improve bidding performance.
I'm thinking of taking the means in the log domain so that they decay naturally to 1 in the
absence of new data points.

\end{document}